/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jsite.common.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsite.common.config.Global;
import com.jsite.common.utils.CookieUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 分页类
 * @author ThinkGem
 * @version 2013-7-2
 * @param <T>
 */
public class Page<T> {
	
	private int pageNumber = 1; // 当前页码
	private int pageSize = Integer.valueOf(Global.getProperty("page.pageSize")); // 页面大小，设置为“-1”表示不进行分页（分页无效）
	
	private long total;// 总记录数，设置为“-1”表示不查询总数
	
	private List<T> rows = new ArrayList<T>();
	
	private String orderBy = ""; // 标准查询有效， 实例： updatedate desc, name asc

	private String sortOrder = "ASC";

	public Page() {
		this.pageSize = -1;
	}
	
	/**
	 * 构造方法
	 * @param request 传递 repage 参数，来记住页码
	 * @param response 用于设置 Cookie，记住页码
	 */
	public Page(HttpServletRequest request, HttpServletResponse response){
		this(request, response, -2);
	}

	/**
	 * 构造方法
	 * @param request 传递 repage 参数，来记住页码
	 * @param response 用于设置 Cookie，记住页码
	 * @param defaultPageSize 默认分页大小，如果传递 -1 则为不分页，返回所有数据
	 */
	public Page(HttpServletRequest request, HttpServletResponse response, int defaultPageSize){
		// 设置页码参数（传递repage参数，来记住页码）
		String no = request.getParameter("pageNumber");
		if (StringUtils.isNumeric(no)){
			CookieUtils.setCookie(response, "pageNumber", no);
			this.setPageNumber(Integer.parseInt(no));
		}else if (request.getParameter("repage")!=null){
			no = CookieUtils.getCookie(request, "pageNumber");
			if (StringUtils.isNumeric(no)){
				this.setPageNumber(Integer.parseInt(no));
			}
		}
		// 设置页面大小参数（传递repage参数，来记住页码大小）
		String size = request.getParameter("pageSize");
		if (StringUtils.isNumeric(size)){
			CookieUtils.setCookie(response, "pageSize", size);
			this.setPageSize(Integer.parseInt(size));
		}else if (request.getParameter("repage")!=null){
			size = CookieUtils.getCookie(request, "pageSize");
			if (StringUtils.isNumeric(size)){
				this.setPageSize(Integer.parseInt(size));
			}
		}else if (defaultPageSize != -2){
			this.pageSize = defaultPageSize;
		}
		// 设置排序参数
		String orderBy = request.getParameter("orderBy");
		if (StringUtils.isNotBlank(orderBy)){
			this.setOrderBy(orderBy);
		}

        String sort = request.getParameter("sortOrder");
        if (StringUtils.isNotBlank(sort)){
            this.setSortOrder(sort);
        }
	}
	
	/**
	 * 构造方法
	 * @param pageNumber 当前页码
	 * @param pageSize 分页大小
	 */
	public Page(int pageNumber, int pageSize) {
		this(pageNumber, pageSize, 0);
	}
	
	/**
	 * 构造方法
	 * @param pageNumber 当前页码
	 * @param pageSize 分页大小
	 * @param total 数据条数
	 */
	public Page(int pageNumber, int pageSize, long total) {
		this(pageNumber, pageSize, total, new ArrayList<T>());
	}
	
	/**
	 * 构造方法
	 * @param pageNumber 当前页码
	 * @param pageSize 分页大小
	 * @param total 数据条数
	 * @param rows 本页数据对象列表
	 */
	public Page(int pageNumber, int pageSize, long total, List<T> rows) {
		this.setTotal(total);
		this.setPageNumber(pageNumber);
		this.pageSize = pageSize;
		this.rows = rows;
	}
	
	/**
	 * 获取设置总数
	 * @return
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * 设置数据总数
	 * @param total
	 */
	public void setTotal(long total) {
		this.total = total;
		if (pageSize >= total){
			pageNumber = 1;
		}
	}
	
	/**
	 * 获取当前页码
	 * @return
	 */
	public int getPageNumber() {
		return pageNumber;
	}
	
	/**
	 * 设置当前页码
	 * @param pageNumber
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	/**
	 * 获取页面大小
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小（最大500）
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;// > 500 ? 500 : pageSize;
	}


	/**
	 * 获取本页数据对象列表
	 * @return List<T>
	 */
	public List<T> getRows() {
		return rows;
	}

	/**
	 * 设置本页数据对象列表
	 * @param rows
	 */
	public Page<T> setRows(List<T> rows) {
		this.rows = rows;
		return this;
	}

	/**
	 * 获取查询排序字符串
	 * @return
	 */
	@JsonIgnore
	public String getOrderBy() {
		// SQL过滤，防止注入 
		String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
					+ "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";
		Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
		if (sqlPattern.matcher(orderBy).find()) {
			return "";
		}
		return orderBy;
	}

	/**
	 * 设置查询排序，标准查询有效， 实例： updatedate desc, name asc
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

    @JsonIgnore
    public String getSortOrder() {
        // SQL过滤，防止注入
        String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
                + "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";
        Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        if (sqlPattern.matcher(sortOrder).find()) {
            return "";
        }
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
	 * 分页是否有效
	 * @return this.pageSize==-1
	 */
	@JsonIgnore
	public boolean isDisabled() {
		return this.pageSize==-1;
	}
	
	/**
	 * 是否进行总数统计
	 * @return this.total==-1
	 */
	@JsonIgnore
	public boolean isNotCount() {
		return this.total ==-1;
	}
	
	/**
	 * 获取 Hibernate FirstResult
	 */
	public int getFirstResult(){
		int firstResult = (getPageNumber() - 1) * getPageSize();
		if (firstResult >= getTotal()) {
			firstResult = 0;
		}
		return firstResult;
	}
	/**
	 * 获取 Hibernate MaxResults
	 */
	public int getMaxResults(){
		return getPageSize();
	}

}
